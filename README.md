# Links for Resources

### .NET

* <a href="http://www.dailycoding.com/posts/convert_image_to_base64_string_and_base64_string_to_image.aspx" target="_blank">Base64Image</a> - Convert Image to Base64 String and Base64 String to Image


### PHP

* <a href="https://www.tools4noobs.com/online_php_functions/base64_encode/" target="_blank">EncodeBase64</a>
* <a href="https://www.tools4noobs.com/online_php_functions/base64_decode/" target="_blank">DecodeBase64</a>


### IMAGEM

* <a href="http://codebeautify.org/base64-to-image-converter" target="_blank">DecodeBase64 Image</a>
* <a href="https://www.jpeg.io" target="_blank">JPEG Image Optimization</a>
* <a href="https://kraken.io/web-interface" target="_blank">Image Optimizer</a>
* <a href="https://apetools.webprofusion.com/app/#/" target="_blank">Icons and Splashscreens for your app</a>
* <a href="http://ticons.fokkezb.nl" target="_blank">Generate Icons & Splash (Launch) Images</a>
* <a href="https://www.remove.bg" target="_blank">Remove Background</a>

### JAVASCRIPT

* <a href="http://youmightnotneedjquery.com" target="_blank">no Jquery</a>

### ANGULAR

### API/REST

* <a href="http://jsoneditoronline.org" target="_blank">JSON Editor Online</a>
* <a href="https://insomnia.rest" target="_blank">Insomnia REST Client</a>

### MARKDOWN

* <a href="https://stackedit.io/" target="_blank">StackEdit Markdown</a>

### TEXT/STRING

* <a href="https://www.functions-online.com/str_replace.html" target="_blank">String Replace</a>
* <a href="http://www.utilities-online.info/xmltojson/" target="_blank">XML -> JSON | JSON -> XML</a>
* <a href="http://codebeautify.org/jsontoxml" target="_blank">JSON -> XML</a>
* <a href="http://meyerweb.com/eric/tools/dencoder/" target="_blank">URL Decoder/Encoder</a>
* <a href="http://codebeautify.org/xmlvalidate" target="_blank">XML validator and Others</a>
* <a href="https://www.easycalculation.com/ascii-hex.php" target="_blank">ASCII Converter and Others</a>
* <a href="http://www.epochconverter.com/" target="_blank">Epoch & Unix Timestamp Conversion Tools</a>
* <a href="http://www.javascriptkit.com/script/script2/charcount.shtml" target="_blank">Calculate and display the number of characters</a>

### TABLES

* <a href="https://uglyduck.ca/responsive-tables/?utm_source=Responsive+Design+Weekly" target="_blank">Flexbox Table</a>

### NODE

### GULP

### OTHERS

* <a href="https://hackernoon.com/67-useful-tools-libraries-and-resources-for-saving-your-time-as-a-web-developer-7d3fb8667030" target="_blank">Useful Tools for Web Developer</a>
* <a href="http://freefrontend.com" target="_blank">FreeFrontend</a>

[Base64Image]: <http://www.dailycoding.com/posts/convert_image_to_base64_string_and_base64_string_to_image.aspx>